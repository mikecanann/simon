

# simon - a memory game


# run the code

in the terminal, type the command:
```
yarn
yarn run start
```

Screenshot of running App:

![app_screenshot](/screen.png "App Screenshot")



# game flow:

turn on the game and press start ( skip power button, and start game on page load?)

* start a turn (page load or after reset button press)
  * add a Sound and it's matching color/button to the sequence
    * if the sequence is > 20 display the 'You Win' message and start the game over - TODO add the 20 move limit
  * play the current sequence
  * wait for user to press buttons to match the sequence
    * change the button color when clicked
    * checking the input to the initial sequence
    * if error
      * strict mode:
        - display error "!!"
        - start the game over 
      * not in strict mode
        - display error
        - restart the round, start playing the sequence again



# tools used

see the package.json


