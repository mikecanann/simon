export const colors = {
  red: '#F92C17',
  blue: '#028CE7',
  green: '#3ED14C',
  yellow: '#FFF329',
  white: '#FFFFFF',
  dark: '#1B1A1A',
}

export const ids = [
  'green',
  'red',
  'yellow',
  'blue',
];

export const TRANSITION_TIME = 200;
export const TRANSITION_TIME_SHORT = 100;
export const TRANSITION_LEVEL = 200;
