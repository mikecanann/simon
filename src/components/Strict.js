import styled from 'styled-components';

const Strict = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  margin: auto;
  marginTop: 130px;
  font-size: 10px;
  border-radius: 5%;
  width: 135px;
  height: 80px;
  line-height: 40px;
  text-align: center;
  z-index: 3;
  padding: 0px;
`;

export default Strict;
