import styled from 'styled-components';
import { withReflex } from 'reflexbox';
import color from 'color';


const lighten = from => color(from).lighten(0.7).hexString();

const activeCSS = props => `
  background-color: ${lighten(props.color)} ;
`;

const Pad = styled.div`
  width: 200px;
  height: 200px;
  box-sizing: border-box;
  background-color: ${({ color }) => color}
  cursor: pointer;
  animation: none;
  transition: 0.2s;
  ${(props) => props.active ? activeCSS(props) : ''}

  @media (max-width: 600px) {
    width: 170px;
    height: 170px;
  }

  &:active {
    background-color: ${({ color }) => lighten(color)}
    ${activeCSS}
  }


  &.top-left {
    border-radius: 320px 10px 10px 10px;
  }

  &.top-right {
    border-radius: 10px 320px 10px 10px;
  }

  &.bottom-left {
    border-radius: 10px 10px 10px 320px;
  }

  &.bottom-right {
    border-radius: 10px 10px 320px 10px;
  }

`;

export default withReflex()(Pad);
