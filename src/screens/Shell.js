import React, { Component } from 'react';
import { Flex } from 'reflexbox';

import { connect } from 'react-redux';

export class Shell extends Component {
  render() {
    const { style, children } = this.props;
    return (
      <Flex align="center" justify="center" style={{ width: '100%', height: '100%', ...style }} >
          {children}
      </Flex>
    );
  }
}

export default connect(
  ({ game }) => game,
)(Shell);
