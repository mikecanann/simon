import React, { Component  } from 'react';
import { Flex } from 'reflexbox';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Game from '../components/Game';
import Score from '../components/Score';
import Smallbackground from '../components/Smallbackground';
import Reset from '../components/Reset';
import Strict from '../components/Strict';
import Pads from '../components/Pads';
import Shell from './Shell';
import { Button } from '../components/Buttons';
import { actionCreators } from '../actions/game';
import {
  TRANSITION_TIME,
  TRANSITION_LEVEL
} from '../constants';
import Player from './Player';


const sleep = (ms = 0) => { return new Promise(r => setTimeout(r, ms)); };


export class Board extends Component {


  componentDidMount() {
    this.startMatch();
  }

  startMatch() {
    const {actions} = this.props;
    actions.startGame();
    sleep(TRANSITION_TIME)
      .then(() => actions.playLevel());
  }

  onStrictClick () {
    const { actions} = this.props;

    actions.toggleStrictMode();
  }

  onPadClick({ id }) {
    const { actions, game, match } = this.props;
    const tail = match.guessed.length;
    const succeeded = match.all[tail] === id;

    if(!game.strictMode) {
      if(!succeeded) {
        // reset the guess back to the first
        actions.replayLevel();
        // play the current level over
        actions.playLevel();
        return;
      }
    }


    if (!game.gameOver) {
      actions.guess({ id, succeeded })
        .then(async ({ done }) => {
          if (done) {
            actions.nextLevel();
            await sleep(TRANSITION_LEVEL);
            actions.playLevel();
          }
        });
    }
  }

  render() {
    const { playingAudio, score, strictMode, gameOver } = this.props.game;
    return (
      <Shell>
        {gameOver &&
          <div>
            <audio  autoPlay >
              <source src="./failSound.ogg" type="audio/ogg" />
              <source src="./failSound.mp3" type="audio/mpeg" />
            </audio>
          </div>
        }
          <Smallbackground>
          </Smallbackground>
          <Game disbledPointer={playingAudio}>
            <Flex
              align="center"
            >
              {this.props.pads.slice(0, 2).map((pad, i) => (
                <Pads key={i} pad={pad} onClick={_ => this.onPadClick({ id: pad.id })} />
              ))}
            </Flex>
            <Strict>
              <Button
                type='button'
                value='Strict'
                className={strictMode ? 'roundButtonRed' : 'roundButton'}
                clickHandler={this.onStrictClick.bind(this)}
              >
              </Button>
              <br/>
            </Strict>
            <Score>{score} </Score>
            <Reset>
              <br/>
              <Button
                type='button'
                value='Reset'
                className='roundButton'
                clickHandler={this.startMatch.bind(this)}
              >
            </Button>
            </Reset>
            <Flex
              align="center"
              justify="center"
            >
              {this.props.pads.slice(2, 4).map((pad, i) => (
                <Pads key={i} pad={pad} onClick={_ => this.onPadClick({ id: pad.id })} />
              ))}
            </Flex>
          </Game>
        <Player />
      </Shell>
    );
  }
}

export default connect(
  state => state,
  dispatch => ({
    actions: bindActionCreators(actionCreators, dispatch),
  }),
)(Board);
