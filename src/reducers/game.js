import {
  START_AUDIO,
  FINISH_AUDIO,
  GUESS_COLOR,
  NEXT_LEVEL,
  START_GAME,
  TOGGLE_STRICT,
} from '../actions/game';

export const initialState = {
  playingAudio: true,
  gameOver: false,
  score: "01",
  strictMode: false,
}

export default function game(state = initialState, action) {
  const { type, payload } = action;
  switch(type) {
    case START_GAME:
      return {
        ...state,
        gameOver: false,
        score: "01",
      }

    case START_AUDIO:
      return {
        ...state,
        playingAudio: true,
      }

    case FINISH_AUDIO:
      return {
        ...state,
        playingAudio: false,
      }

    case NEXT_LEVEL:
      return {
        ...state,
        score: ("00" + (parseInt(state.score, 10) + 1)).slice(-2) ,
      }
    case TOGGLE_STRICT:
      return {
        ...state,
        strictMode: !state.strictMode,
      }

    case GUESS_COLOR:
      return {
        ...state,
        gameOver: !payload.succeeded,
        score: payload.succeeded ? state.score : "!!",
      }

    default:
      return state;
  }
}
