import {
  TRANSITION_TIME,
  TRANSITION_TIME_SHORT,
} from '../constants';

import { ids } from '../constants';

export const START_GAME    = 'START_GAME';
export const START_AUDIO   = 'START_AUDIO';
export const FINISH_AUDIO  = 'FINISH_AUDIO';
export const PAD_PRESS     = 'PAD_PRESS';
export const PAD_RELEASE   = 'PAD_RELEASE';
export const GUESS_COLOR   = 'GUESS_COLOR';
export const NEXT_LEVEL    = 'NEXT_LEVEL';
export const TOGGLE_STRICT = 'TOGGLE_STRICT';
export const REPLAY_LEVEL  = 'REPLAY_LEVEL';


function createAction(type) {
  return (payload = {}) => ({
    type,
    payload,
  });
}

const start = createAction(START_GAME);
const next  = createAction(NEXT_LEVEL);

const toggleStrictMode = createAction(TOGGLE_STRICT);


const getRandomId = () => ids[Math.floor(Math.random() * ids.length)];

const startGame   = payload => start({ next: getRandomId() });
const nextLevel   = payload => next({ next: getRandomId() });
const replayLevel = createAction(REPLAY_LEVEL);

const guessColor  = createAction(GUESS_COLOR);
const startAudio  = createAction(START_AUDIO);
const finishAudio = createAction(FINISH_AUDIO);
const padPress    = createAction(PAD_PRESS);
const padRelease  = createAction(PAD_RELEASE);

const sleep = (ms = 0) => { return new Promise(r => setTimeout(r, ms)); };

const playLevel = (payload) => async (dispatch, getState) => {

  // play through the current level
  dispatch(startAudio());
  const { match } = getState();
  for (let i = 0; i <= match.all.length - 1; i++) {
    const id = match.all[i];
    dispatch(padPress({ id }));
    await sleep(TRANSITION_TIME);
    dispatch(padRelease());
    await sleep(TRANSITION_TIME);
  }

  dispatch(finishAudio());
}


const guess = ({ succeeded, id }) => async (dispatch, getState) => {

  // press the button and play the audio
  dispatch(guessColor({ succeeded, id }));
  dispatch(startAudio());
  dispatch(padPress({ id }));
  await sleep(TRANSITION_TIME_SHORT);

  // release button
  dispatch(padRelease());
  await sleep(TRANSITION_TIME_SHORT);
  dispatch(finishAudio());

  const { match } = getState();
  const { all, guessed } = match;
  const done = (all.length === guessed.length) && succeeded;

  return new Promise(r => r({ done }));
}

export const actionCreators = {
  startAudio,
  startGame,
  finishAudio,
  padPress,
  padRelease,
  nextLevel,
  replayLevel,
  guessColor,
  guess,
  playLevel,
  toggleStrictMode,
}
